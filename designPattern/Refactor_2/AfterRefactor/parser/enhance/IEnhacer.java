public interface PnrTreeEnhancer {
    public void enhance(PnrTree pnrTree, RawContent rawContent, Set<SubjectArea> areas, View view) throws Exception;
}
