public class FopDataPnrTreeEnhancer implements PnrTreeEnhancer {

	private Logger logger = LoggerFactory.getLogger(FopDataPnrTreeEnhancer.class);

	List<IRemarkToLbtFopDataConverter> converters = new ArrayList<>();

	@Override
	public void enhance(PnrTree pnrTree, RawContent rawContent, Set<SubjectArea> areas, View view) throws Exception {
		logger.debug("Enhancing PnrTree with FoPs");
		checkNotNull(pnrTree, "Empty PNRTree");
		try {
			List<FopData> fopDataList = extractFopData(rawContent);
			OpenReservation openReservation = extractOpenReservation(rawContent);
			attachApprovalFromOpenResToFopRow(openReservation, fopDataList);
			pnrTree.formsOfPayment.clear();
			pnrTree.formsOfPayment.addAll(translateFopData(pnrTree, fopDataList));
		} catch (RuntimeException e) {
			logger.error("Error while enhancing with Form of Payments", e);
		}
	}

	private List<FormOfPaymentData> translateFopData(PnrTree pnrTree, List<FopData> fopDataList) {
		return FluentIterable.from(pnrTree.getRemarks()).filter(getFormOfPaymentPredicate())
				.transform(new RemarkToFopFunction(fopDataList)).filter(Predicates.notNull()).toList();
	}

	private FormOfPaymentData remarkToFormOfPayment(RemarkData remark, List<FopData> fopDataList) {
		long uid = new BigInteger(+1, remark.getUniqueIdentifier()).longValue();
		if (fopDataList.isEmpty()) {
			return transformOtherFop(remark.getRemarkText());
		}
		FopRow fopRow = findFopRowForRemarkId(fopDataList, uid);
		return null != fopRow ? transformCreditCardDetails(fopRow) : transformOtherFop(remark.getRemarkText());
	}

	private FormOfPaymentData transformOtherFop(String remarkText) {
		for (IRemarkToLbtFopDataConverter converter : getConverters()) {
			if (converter.isApplicable(remarkText)) {
				return converter.createFormOfPaymentData(remarkText);
			}
		}
		return createOtherFop(remarkText);
	}

	private FormOfPaymentData createOtherFop(String remarkText) {
		FormOfPaymentData fop = new FormOfPaymentData();
		OtherFop otherFop = new OtherFop();
		otherFop.setOther(remarkText);
		fop.setOtherFop(otherFop);
		return fop;
	}

	private Predicate<RemarkData> getFormOfPaymentPredicate() {
		return new Predicate<RemarkData>() {
			@Override
			public boolean apply(RemarkData input) {
				return RemarkParser.SBFORMOFPAYMENT == input.getRemarkType();
			}
		};
	}

	private FopRow findFopRowForRemarkId(List<FopData> fopDataList, long remarkId) {
		//@formatter:off
        Optional<FopRow> result = FluentIterable
                .from(fopDataList)
                .filter(Predicates.<FopData>notNull())
                .transformAndConcat(new Function<FopData, List<FopRow>>() {
                    @Override
                    public List<FopRow> apply(FopData input) {
                        return input.getFopRow();
                    }
                })
                .firstMatch(getRemarkIdPredicate(remarkId));
        //@formatter:on
		return result.orNull();
	}

	private Predicate<FopRow> getRemarkIdPredicate(final long remarkId) {
		return new Predicate<FopRow>() {
			@Override
			public boolean apply(FopRow input) {
				try {
					return remarkId == Long.parseLong(input.getRemarkId());
				} catch (Exception ex) {
					// This exception is intentionally eaten
					return false;
				}
			}
		};
	}

	private FormOfPaymentData transformCreditCardDetails(FopRow fopRow) {
		CreditCardDetailsData ccItem = new CreditCardDetailsData();
		ccItem.setIssuerIdentificationNumber(fopRow.getIssuerIdentificationNumber());
		ccItem.setCreditCardCode(fopRow.getIssuerVendorCode());
		ccItem.setCreditCardNumber(fopRow.getCreditCardNumber());
		ccItem.setExpirationMonth(fopRow.getExpirationMonth());
		ccItem.setExpirationYear(fopRow.getExpirationYear());
		ccItem.setExtendedPayment(fopRow.getExtendedPayment());
		ccItem.setSuppressFromInvoice(BooleanUtils.isTrue(fopRow.isSuppressFromInvoice()));
		ccItem.setGenerateApprovalAtTicketing(BooleanUtils.isTrue(fopRow.isGenerateApprovalAtTicketing()));
		ccItem.setbNumber(fopRow.getBNumber());
		createApprovals(ccItem, fopRow);
		FormOfPaymentData fopData = new FormOfPaymentData();
		fopData.setCreditCardDetailsList(Arrays.asList(ccItem));
		return fopData;
	}

	private void createApprovals(CreditCardDetailsData ccItem, FopRow fopRow) {
		List<com.sabre.pnrData.Approval> ccApprovalList = new ArrayList<>();
		if (hasFopApprovalRow(fopRow)) {
			List<Approval> approvals = fopRow.getFopApprovalRow().getApproval();
			for (Approval approval : approvals) {
				com.sabre.pnrData.Approval ccApproval = new com.sabre.pnrData.Approval();
				ccApproval.setManualApproval(approval.isManualApproval());
				ccApproval.setResponseCode(approval.getResponseCode());
				ccApproval.setApprovalCode(approval.getApprovalCode());
				ccApproval.setRequestTime(approval.getRequestTime());
				ccApproval.setExpiryTime(approval.getExpiryTime());
				ccApproval.setAirlineCode(approval.getAirlineCode());
				ccApproval.setAmount(approval.getAmount().getValue().toString());
				ccApproval.setCurrencyCode(approval.getAmount().getCurrencyCode());
				ccApproval.setRemarks(approval.getRemarks());
				ccApproval.setPaymentRef(approval.getPaymentRef());
				ccApproval.setCscMatched(approval.getCSCMatched());
				ccApproval.setCscRemark(approval.getCSCMatched());
				ccApproval.setQualifier(approval.getQualifier());
				ccApproval.setExtendedPayment(approval.getExtendPayment());
				ccApprovalList.add(ccApproval);
			}
			ccItem.setApprovalList(ccApprovalList);
		}
	}

	private boolean hasFopApprovalRow(FopRow fopRow) {
		return fopRow.getFopApprovalRow() != null && !CollectionUtils.isEmpty(fopRow.getFopApprovalRow().getApproval());
	}

	private void attachApprovalFromOpenResToFopRow(OpenReservation openReservation, List<FopData> fopDataList) {
		if (openReservation != null) {
			List<FopRow> fopRows = fopDataList.get(0).getFopRow();
			List<Element> elements = openReservation.getElements();
			List<PaymentCard> paymentCardList = getPaymentCardList(elements);
			createApprovalForFopRow(paymentCardList, fopRows);
		}
	}

	private void createApprovalForFopRow(List<PaymentCard> paymentCardList, List<FopRow> fopRows) {
		for (int i = 0; i < paymentCardList.size(); i++) {
			FopApprovalRow fopApprovalRow = new FopApprovalRow();
			PaymentCard paymentCard = paymentCardList.get(i);

			boolean canAttachApproval = lookForPaymentCardApproval(paymentCard);
			if (canAttachApproval) {
				fopApprovalRow.setApproval(createApprovals(paymentCard));
			}
			fopRows.get(i).withFopApprovalRow(fopApprovalRow);
		}
	}

	private List<Approval> createApprovals(PaymentCard paymentCard) {
		List<Approval> approvals = new ArrayList<>();
		List<PaymentCardApproval> paymentCardApprovalList = paymentCard.getApprovalList().getApprovals();
		for (PaymentCardApproval paymentCardApproval : paymentCardApprovalList) {
			Approval approval = new Approval();
			if (paymentCardApproval.getAmount() != null) {
				Approval.Amount amount = new Approval.Amount();
				amount.setValue(paymentCardApproval.getAmount().getValue());
				amount.withCurrencyCode(paymentCardApproval.getAmount().getCurrencyCode());
				approval.setAmount(amount);
			}
			approval.setResponseCode(paymentCardApproval.getResponseCode());
			approval.setApprovalCode(paymentCardApproval.getApprovalCode());
			approval.setAirlineCode(paymentCardApproval.getAirlineCode());
			approval.setExpiryTime(MappingUtils.formatDateTime(paymentCardApproval.getExpiryTime()));
			approval.setRequestTime(MappingUtils.formatDateTime(paymentCardApproval.getRequestTime()));
			approval.setManualApproval(paymentCardApproval.isManualApproval());
			approval.setRemarks(paymentCardApproval.getRemarks());
			approval.setSupplierTransID(paymentCardApproval.getSupplierTransID());
			approval.setPaymentRef(paymentCardApproval.getPaymentRef());
			approval.setCSCMatched(paymentCardApproval.getCSCMatched());
			approval.setCSCRemark(paymentCardApproval.getCSCRemark());
			approval.setQualifier(paymentCardApproval.getQualifier());
			approval.setExtendPayment(paymentCardApproval.getExtendPayment());
			approvals.add(approval);
		}
		return approvals;
	}

	private boolean lookForPaymentCardApproval(PaymentCard paymentCard) {
		return paymentCard.getApprovalList() != null && paymentCard.getApprovalList().getApprovals() != null
				&& paymentCard.getApprovalList().getApprovals().size() > 0;
	}

	private List<PaymentCard> getPaymentCardList(List<Element> elements) {
		//@formatter:off
		return FluentIterable
				.from(elements)
				.filter(getPaymentCardPredicate())
				.transform(getPaymentCardFromElement())
				.toList();
		//@formatter:on
	}

	private List<FopData> extractFopData(RawContent rawContent) {
		if (null == rawContent) {
			logger.error("RawContent is not provided");
			return new ArrayList<>();
		}
		List<XmlPnrContent> xmlPnrContentList = rawContent.getXmlPnrContent();

		//@formatter:off
        return FluentIterable
                .from(xmlPnrContentList)
                .filter(Predicates.<XmlPnrContent>notNull())
                .filter(getFopDataPredicate())
                .transform(getXMLPnrContentToFopFunction())
                .toList();
        //@formatter:on
	}

	private OpenReservation extractOpenReservation(RawContent rawContent) {
		return OpenReservationHelper.retrieveOpenReservationFromXMLContent(rawContent.getXmlPnrContent());
	}

	private Function<XmlPnrContent, FopData> getXMLPnrContentToFopFunction() {
		return new Function<XmlPnrContent, FopData>() {
			@Override
			public FopData apply(XmlPnrContent input) {
				return returnWithLogFop((FopData) input.getAny());
			}
		};
	}

	private Function<Element, PaymentCard> getPaymentCardFromElement() {
		return new Function<Element, PaymentCard>() {
			@Override
			public PaymentCard apply(Element element) {
				return element.getFormOfPayment().getPaymentCard();
			}
		};
	}

	private Predicate<Element> getPaymentCardPredicate() {
		return new Predicate<Element>() {
			@Override
			public boolean apply(Element element) {
				return element != null && element.getFormOfPayment() != null
						&& element.getFormOfPayment().getPaymentCard() != null;
			}
		};
	}

	private Predicate<XmlPnrContent> getFopDataPredicate() {
		return new Predicate<XmlPnrContent>() {
			@Override
			public boolean apply(XmlPnrContent input) {
				return input.getAny() instanceof FopData;
			}
		};
	}

	private FopData returnWithLogFop(FopData fopData) {
		if (logger.isDebugEnabled()) {
			logger.debug("{}", fopData);
			for (FopRow fopRow : fopData.getFopRow()) {
				logger.debug("fopRow={}{} {}", fopRow.getIssuerVendorCode(), fopRow.getCreditCardNumber(),
						fopRow.getExpirationMonth());
			}
		}
		return fopData;
	}

	public void setConverters(List<IRemarkToLbtFopDataConverter> converters) {
		if (null != converters) {
			this.converters = converters;
		}
	}

	public List<IRemarkToLbtFopDataConverter> getConverters() {
		return converters;
	}

	private class RemarkToFopFunction implements Function<RemarkData, FormOfPaymentData> {

		final List<FopData> fopDataList;

		RemarkToFopFunction(List<FopData> fopDataList) {
			this.fopDataList = fopDataList;
		}

		@Override
		public FormOfPaymentData apply(RemarkData remark) {
			return remarkToFormOfPayment(remark, fopDataList);
		}
	}

}
