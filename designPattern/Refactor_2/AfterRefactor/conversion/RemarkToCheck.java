package com.sabre.tds.pnr.conversion;

import com.sabre.pnrData.Check;
import com.sabre.pnrData.FormOfPaymentData;


public class RemarkToCheck implements IRemarkToLbtFopDataConverter {
	private static final String CHECK_REGEX = "(CK|CHECK|CHEQUE){1}(\\.B[0-9A-Z]*)?";
	@Override
	public FormOfPaymentData createFormOfPaymentData(String remarkText) {
		FormOfPaymentData cashFormOfPaymentData = new FormOfPaymentData();
		if(isApplicable(remarkText)){
			cashFormOfPaymentData.setCheckFop(createMatchedTypePayment(remarkText));
		}
		return cashFormOfPaymentData;
	}

	private Check createMatchedTypePayment(String remarkText) {
		Check check = new Check();
		int i = remarkText.indexOf(B_NUMBER_PREFIX);
		if(i < 0){
			check.setCheck(remarkText);
		}else{
			check.setCheck(remarkText.substring(0,i));
			check.setBnumber(remarkText.substring(i+2));
		}
		return check;
	}

	@Override
	public boolean isApplicable(String remarkText) {
		return remarkText !=null && remarkText.matches(CHECK_REGEX);
	}
}
