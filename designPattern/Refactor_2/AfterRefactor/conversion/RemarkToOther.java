package com.sabre.tds.pnr.conversion;

import com.sabre.pnrData.FormOfPaymentData;
import com.sabre.pnrData.OtherFop;

public class RemarkToOther implements IRemarkToLbtFopDataConverter {
	private static final String INVOICE_REGEX = "(AGTINVOICE|AGTINV|AGT INVOICE|AGT INV|INVOICE|INV){1}(\\.B[0-9A-Z]*)?";
	@Override
	public FormOfPaymentData createFormOfPaymentData(String remarkText) {
		FormOfPaymentData cashFormOfPaymentData = new FormOfPaymentData();
		if(isApplicable(remarkText)){
			cashFormOfPaymentData.setOtherFop(createMatchedTypePayment(remarkText));
		}
		return cashFormOfPaymentData;
	}

	private OtherFop createMatchedTypePayment(String remarkText) {
		OtherFop OtherFop = new OtherFop();
		int i = remarkText.indexOf(B_NUMBER_PREFIX);
		if(i < 0){
			OtherFop.setOther(remarkText);
		}else{
			OtherFop.setOther(remarkText.substring(0,i));
			OtherFop.setBnumber(remarkText.substring(i+2));
		}
		return OtherFop;

	}

	@Override
	public boolean isApplicable(String remarkText) {
		return remarkText != null && remarkText.matches(INVOICE_REGEX);
	}
}
