public interface Format {
	public Object parseToJAXBObject(Object any, Set<SubjectArea> subjectAreas, SearchView view, TransactionContext transactionContext);
}
