package com.sabre.tds.pnr.conversion;

import com.sabre.pnrData.FormOfPaymentData;

import java.util.List;

public class RemarkToLbtFopDataConverterImpl implements IRemarkToLbtFopDataConverter {
	private List<IRemarkToLbtFopDataConverter> converters;

	public RemarkToLbtFopDataConverterImpl() {
	}

	@Override
	public FormOfPaymentData createFormOfPaymentData(String remarkText) {
		if (isApplicable(remarkText)) {
			for (IRemarkToLbtFopDataConverter converter : converters) {
				if (converter.isApplicable(remarkText)) {
					return converter.createFormOfPaymentData(remarkText);
				}
			}
		}
		return null;
	}

	@Override
	public boolean isApplicable(String remarkText) {
		return (remarkText != null && converters != null);
	}

	public RemarkToLbtFopDataConverterImpl(List<IRemarkToLbtFopDataConverter> converters) {
		this.converters = converters;
	}
}
