public class LbtFormatImpl implements Format {

	private static Logger logger = Logger.getLogger(LbtFormatImpl.class);

	public static final String ADD_PARTNERSHIP_AIRLINES_IND = "XXX";
	public static final String ADD_POC_IND = "XXX";

	private String lbtVersion;

	@Autowired
	private BusClient busClient;

	@Autowired
	@Qualifier(value = "remarkConverter")
	IRemarkToLbtFopDataConverter converter;

	private void populateAdditionalData(PnrTree pnrTree, Set<SubjectArea> subjectAreas, View view)
			throws com.sabre.sp.bus.exception.ServiceException {

		populatePoc(pnrTree, view);
		populatePartnershipAirlines(pnrTree, view);
	}

	protected void populatePartnershipAirlines(PnrTree pnrTree, View view)
			throws com.sabre.sp.bus.exception.ServiceException {

		if (view != null && view.getIndicators() != null
				&& view.getIndicators().getIndicator().contains(ADD_PARTNERSHIP_AIRLINES_IND)) {

			for (FrequentFlyerData ffData : pnrTree.frequentFlyers) {
				if (ffData.getVendor() == null || "".equals(ffData.getVendor())) {
					logger.warn(pnrTree.headerData.getLocator() + ": Could not populate Partnership Airline Codes,"
							+ " FrequentFlyerData does not contain vendor information.");
					continue;
				}

				ServiceId partnershipDataService = new ServiceId("PartnershipDataProviderRQ", "1.0.0");

				PartnershipDataProviderRQ partnershipDataProviderRQ = new PartnershipDataProviderRQ();
				partnershipDataProviderRQ.setDomainId(pnrTree.headerData.getPnrPartition());
				partnershipDataProviderRQ.setAirlineCode(ffData.getVendor());

				Message<PartnershipDataProviderRQ> partnershipDataMessage = MessageBuilder
						.createRequestMessageBuilder(partnershipDataService).withPayload(partnershipDataProviderRQ)
						.create();

				@SuppressWarnings("unchecked")
				Message<PartnershipDataProviderRS> out = (Message<PartnershipDataProviderRS>) busClient
						.invokeService(partnershipDataService, partnershipDataMessage);

				PartnershipDataProviderRS partnershipDataProviderRS = out.getPayload();

				logger.debug("partnership airline codes:" + partnershipDataProviderRS.getAirlineCode());

				ffData.setPartnershipAirlineCodes(partnershipDataProviderRS.getAirlineCode());
			}
		}
	}

	private void populatePoc(PnrTree pnrTree, View view) {

		if (view != null && view.getIndicators() != null && view.getIndicators().getIndicator() != null
				&& view.getIndicators().getIndicator().contains(ADD_POC_IND)) {

			String pocAirport = (String) ((view instanceof SearchView)
					? ((SearchView) view).getParameter(StlContext.POC_AIRPORT_PARAM) : null);
			Calendar pocDeparture = (Calendar) ((view instanceof SearchView)
					? ((SearchView) view).getParameter(StlContext.POC_DEPARTURE_PARAM) : null);
			PocData pocData = PocUtils.getPoc(pnrTree, pocAirport, pocDeparture);
			if (pocData != null) {
				pnrTree.setPocAirport(pocData.getAirport());
				pnrTree.setPocDeparture(pocData.getDeparture().getTime());
			}
		}
	}

	public Object parse(Object any, Set<SubjectArea> areas, View view) {
		byte[] content = null;
		List<FopData> fopDataList = null;

		if (logger.isDebugEnabled()) {
			logger.debug("before parse");
			logger.debug("any:" + any);
			logger.debug("areas" + areas);
		}

		if (any instanceof RawContent) {
			RawContent rawContent = (RawContent) any;
			content = rawContent.getContent();
			fopDataList = extractFopData(rawContent);
			throwIfNull(content, ReaccErrors.PNR_NO_DATA);
		} else if (any instanceof com.sabre.tds.itinerary.RawContent) {
			content = ((com.sabre.tds.itinerary.RawContent) any).getContent();
			throwIfNull(content, ReaccErrors.PNR_NO_DATA);
		}

		if (content != null) {
			try {
				PnrTree pnrTree = parseTree(content, areas);

				populateAdditionalData(pnrTree, areas, view);
				populateFopData(pnrTree, fopDataList);

				IBindingFactory bfact = BindingDirectory.getFactory(PNRWrapper.class);
				IMarshallingContext marshallingContext = bfact.createMarshallingContext();

				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

				PNRWrapper wrapper = new PNRWrapper();
				wrapper.setPnrData(pnrTree);

				logger.debug("pnrWrapper: " + wrapper.getPnrData().toString());
				marshallingContext.marshalDocument(wrapper, "UTF-8", null, byteArrayOutputStream);

				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);

				Document document = dbf.newDocumentBuilder()
						.parse(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

				return document.getDocumentElement();
			} catch (Exception e) {
				throw new ServiceException(ReaccErrors.PNR_PARSING_ERROR, e);
			}
		}
		return any;
	}

	private void populateFopData(PnrTree pnrTree, List<FopData> fopDataList) {
		pnrTree.formsOfPayment.clear();
		List<RemarkData> remarks = pnrTree.getRemarks();
		for (RemarkData remark : remarks) {
			if (remark.getRemarkType() == RemarkParser.SBFORMOFPAYMENT) {
				long uid = new BigInteger(+1, remark.getUniqueIdentifier()).longValue();
				FopRow fopRow = findFopRowforRemarkId(fopDataList, uid);
				if (fopRow != null) {
					injectCreditCardDetails(pnrTree, fopRow);
				} else {
					injectFormOfPaymentData(pnrTree,remark);
				}
			}
		}
	}

	private void injectFormOfPaymentData(PnrTree pnrTree,RemarkData remark){
		FormOfPaymentData formOfPaymentData = converter.createFormOfPaymentData(remark.getRemarkText());
		if (formOfPaymentData != null) {
			pnrTree.formsOfPayment.add(formOfPaymentData);
		} else {
			FormOfPaymentData fopOther = createOtherFop(remark.getRemarkText());
			pnrTree.formsOfPayment.add(fopOther);
		}
	}

	private FormOfPaymentData createOtherFop(String remarkText) {
		OtherFop otherFop = new OtherFop();
		otherFop.setOther(remarkText);
		FormOfPaymentData fopOther = new FormOfPaymentData();
		fopOther.setOtherFop(otherFop);
		return fopOther;
	}

	private FopRow findFopRowforRemarkId(List<FopData> fopDataList, long remarkId) {
		if (fopDataList != null) {
			for (FopData fopData : fopDataList) {
				if (fopData.getFopRow() != null && fopData.getFopRow().size() > 0) {
					for (FopData.FopRow fopRow : fopData.getFopRow()) {
						if (fopRow != null) {
							try {
								long fopRowRemarkId = Long.parseLong(fopRow.getRemarkId());
								if (remarkId == fopRowRemarkId) {
									return fopRow;
								}
							} catch (Exception ex) {
								// This exception is intentionally eaten
							}
						}
					}
				}
			}
		}
		return null;
	}

	private void injectCreditCardDetails(PnrTree pnrTree, FopRow fopRow) {
		CreditCardDetailsData ccItem = new CreditCardDetailsData();
		ccItem.setIssuerIdentificationNumber(fopRow.getIssuerIdentificationNumber());
		ccItem.setCreditCardCode(fopRow.getIssuerVendorCode());
		ccItem.setCreditCardNumber(fopRow.getCreditCardNumber());
		ccItem.setExpirationMonth(fopRow.getExpirationMonth());
		ccItem.setExpirationYear(fopRow.getExpirationYear());
		ccItem.setExtendedPayment(fopRow.getExtendedPayment());
		ccItem.setSuppressFromInvoice(Boolean.TRUE.equals(fopRow.isSuppressFromInvoice()));
		ccItem.setGenerateApprovalAtTicketing(Boolean.TRUE.equals(fopRow.isGenerateApprovalAtTicketing()));
		ccItem.setbNumber(fopRow.getBNumber());
		createApprovals(ccItem, fopRow);
		FormOfPaymentData fopData = new FormOfPaymentData();
		fopData.setCreditCardDetailsList(Arrays.asList(ccItem));
		pnrTree.formsOfPayment.add(fopData);
	}

	private void createApprovals(CreditCardDetailsData ccItem, FopRow fopRow) {
		List<com.sabre.pnrData.Approval> ccApprovalList = new ArrayList<>();
		if (hasFopApprovalRow(fopRow)) {
			List<Approval> approvals = fopRow.getFopApprovalRow().getApproval();
			for (Approval approval : approvals) {
				com.sabre.pnrData.Approval ccApproval = new com.sabre.pnrData.Approval();
				ccApproval.setManualApproval(approval.isManualApproval());
				ccApproval.setResponseCode(approval.getResponseCode());
				ccApproval.setApprovalCode(approval.getApprovalCode());
				ccApproval.setRequestTime(approval.getRequestTime());
				ccApproval.setExpiryTime(approval.getExpiryTime());
				ccApproval.setAirlineCode(approval.getAirlineCode());
				ccApproval.setAmount(approval.getAmount().getValue().toString());
				ccApproval.setCurrencyCode(approval.getAmount().getCurrencyCode());
				ccApproval.setRemarks(approval.getRemarks());
				ccApproval.setPaymentRef(approval.getPaymentRef());
				ccApproval.setCscMatched(approval.getCSCMatched());
				ccApproval.setCscRemark(approval.getCSCMatched());
				ccApproval.setQualifier(approval.getQualifier());
				ccApproval.setExtendedPayment(approval.getExtendPayment());
				ccApprovalList.add(ccApproval);
			}
			ccItem.setApprovalList(ccApprovalList);
		}
	}

	private boolean hasFopApprovalRow(FopRow fopRow) {
		return fopRow.getFopApprovalRow() != null && !CollectionUtils.isEmpty(fopRow.getFopApprovalRow().getApproval());
	}

	private List<FopData> extractFopData(RawContent rawContent) {
		List<FopData> fopDataList = new ArrayList<>();
		List<XmlPnrContent> xmlPnrContentList = rawContent.getXmlPnrContent();
		final OpenReservation openReservation = OpenReservationHelper
				.retrieveOpenReservationFromXMLContent(xmlPnrContentList);
		if ((xmlPnrContentList != null) && (xmlPnrContentList.size() > 0)) {
			for (XmlPnrContent xmlPnrContent : xmlPnrContentList) {
				if (xmlPnrContent != null && xmlPnrContent.getAny() instanceof FopData) {
					final FopData fopData = (FopData) xmlPnrContent.getAny();
					logFop(fopData);
					if (openReservation != null) {
						logOpenRes(openReservation);
						getApprovalsFromOpenResToFopData(openReservation, fopData);
					}
					fopDataList.add(fopData);
				}
			}
		}
		return (fopDataList.size() > 0) ? fopDataList : null;
	}

	private void getApprovalsFromOpenResToFopData(OpenReservation openReservation, FopData fopData) {
		List<Element> elements = openReservation.getElements();
		List<FopRow> fopRows = fopData.getFopRow();
		List<PaymentCard> paymentCardList = getPaymentCardList(elements);

		for (int i = 0; i < paymentCardList.size(); i++) {
			FopApprovalRow fopApprovalRow = new FopApprovalRow();
			PaymentCard paymentCard = paymentCardList.get(i);

			boolean canAttachApproval = lookForPaymentCardApproval(paymentCard);
			if (canAttachApproval) {
				fopApprovalRow.setApproval(createApprovals(paymentCard));
			}
			fopRows.get(i).withFopApprovalRow(fopApprovalRow);
		}
	}

	private List<Approval> createApprovals(PaymentCard paymentCard) {
		List<Approval> approvals = new ArrayList<>();
		List<PaymentCardApproval> paymentCardApprovalList = paymentCard.getApprovalList().getApprovals();
		for (PaymentCardApproval paymentCardApproval : paymentCardApprovalList) {
			Approval approval = new Approval();
			if (paymentCardApproval.getAmount() != null) {
				Approval.Amount amount = new Approval.Amount();
				amount.setValue(paymentCardApproval.getAmount().getValue());
				amount.withCurrencyCode(paymentCardApproval.getAmount().getCurrencyCode());
				approval.setAmount(amount);
			}
			approval.setResponseCode(paymentCardApproval.getResponseCode());
			approval.setApprovalCode(paymentCardApproval.getApprovalCode());
			approval.setAirlineCode(paymentCardApproval.getAirlineCode());
			approval.setExpiryTime(MappingUtils.formatDateTime(paymentCardApproval.getExpiryTime()));
			approval.setRequestTime(MappingUtils.formatDateTime(paymentCardApproval.getRequestTime()));
			approval.setManualApproval(paymentCardApproval.isManualApproval());
			approval.setRemarks(paymentCardApproval.getRemarks());
			approval.setSupplierTransID(paymentCardApproval.getSupplierTransID());
			approval.setPaymentRef(paymentCardApproval.getPaymentRef());
			approval.setCSCMatched(paymentCardApproval.getCSCMatched());
			approval.setCSCRemark(paymentCardApproval.getCSCRemark());
			approval.setQualifier(paymentCardApproval.getQualifier());
			approval.setExtendPayment(paymentCardApproval.getExtendPayment());
			approvals.add(approval);
		}
		return approvals;
	}

	private List<PaymentCard> getPaymentCardList(List<Element> elements) {
		List<PaymentCard> paymentCardLists = new ArrayList<>();
		for (Element element : elements) {
			FormOfPayment formOfPayment = element.getFormOfPayment();
			if (formOfPayment != null && formOfPayment.getPaymentCard() != null) {
				paymentCardLists.add(formOfPayment.getPaymentCard());
			}
		}
		return paymentCardLists;
	}

	private boolean lookForPaymentCardApproval(PaymentCard paymentCard) {
		return paymentCard.getApprovalList() != null && paymentCard.getApprovalList().getApprovals() != null
				&& paymentCard.getApprovalList().getApprovals().size() > 0;
	}

	private void logFop(FopData fopData) {
		if (logger.isDebugEnabled()) {
			logger.debug(fopData);
			for (FopRow fopRow : fopData.getFopRow()) {
				logger.debug("fopRow=" + fopRow.getIssuerVendorCode() + fopRow.getCreditCardNumber() + " "
						+ fopRow.getExpirationMonth());
			}
		}
	}

	private void logOpenRes(OpenReservation openReservation) {
		if (logger.isDebugEnabled()) {
			logger.debug(openReservation);
			for (Element element : openReservation.getElements()) {
				logger.debug("FormofPayment= " + element.getFormOfPayment());
			}
		}
	}

	public PnrTree parseTree(byte[] pnr, Set<SubjectArea> subjectAreas) {
		try {
			PnrTree pnrTree = new PnrTree();
			if (subjectAreas != null) {
				pnrTree.fromTpfdf(pnr, subjectAreas);
			} else {
				pnrTree.fromTpfdf(pnr);
			}
			pnrTree.setMaskCCData(true);
			return pnrTree;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public String version() {
		return lbtVersion;
	}

	@Required
	public void setLbtVersion(String lbtVersion) {
		this.lbtVersion = lbtVersion;
	}

	public boolean isCvsEnabled() {
		return false;
	}

	public Object parse(Object object, View view) {
		return parse(object, null, view);
	}

	@Override
	public Object parseToJAXBObject(Object any, Set<SubjectArea> subjectAreas, SearchView view,
			TransactionContext transactionContext) {
		return parse(any, subjectAreas, view);
	}

	public BusClient getBusClient() {
		return busClient;
	}

	public void setBusClient(BusClient busClient) {
		this.busClient = busClient;
	}

}
