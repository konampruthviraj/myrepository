package com.sabre.tds.pnr.conversion;

import com.sabre.pnrData.Cash;
import com.sabre.pnrData.FormOfPaymentData;

public class RemarkToCash implements IRemarkToLbtFopDataConverter{
	private static final String CASH_REGEX = "(CASH|CA){1}(\\.B[0-9A-Z]*)?";

	@Override
	public FormOfPaymentData createFormOfPaymentData(String remarkText) {
		FormOfPaymentData cashFormOfPaymentData = new FormOfPaymentData();
		if(isApplicable(remarkText)){
			cashFormOfPaymentData.setCashFop(createMatchedTypePayment(remarkText));
		}
		return cashFormOfPaymentData;
	}

	private Cash createMatchedTypePayment(String remarkText) {
		Cash cash = new Cash();
		int i = remarkText.indexOf(B_NUMBER_PREFIX);
		if(i < 0){
			cash.setCash(remarkText);
		}else{
			cash.setCash(remarkText.substring(0,i));
			cash.setBnumber(remarkText.substring(i+2));
		}
		return cash;
	}

	@Override
	public boolean isApplicable(String remarkText) {
		return remarkText != null && remarkText.matches(CASH_REGEX);
	}
}
