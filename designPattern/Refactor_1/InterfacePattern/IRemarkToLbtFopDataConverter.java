package com.sabre.tds.pnr.conversion;

import com.sabre.pnrData.FormOfPaymentData;

public interface IRemarkToLbtFopDataConverter {
	public static final String B_NUMBER_PREFIX = ".B";

	public FormOfPaymentData createFormOfPaymentData(String remarkText);
	public boolean isApplicable(String remarkText);

}
