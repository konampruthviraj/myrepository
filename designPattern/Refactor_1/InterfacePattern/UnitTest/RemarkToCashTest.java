package com.sabre.tds.pnr.conversion;

import org.junit.Before;
import org.junit.Test;

import com.sabre.pnrData.Cash;
import com.sabre.pnrData.FormOfPaymentData;

import junit.framework.Assert;

public class RemarkToCashTest {

	private RemarkToCash converter;
	private FormOfPaymentData formOfPaymentData;
	private Cash expected;
	private Cash actual;

	@Before
	public void setUp() throws Exception {
		converter = new RemarkToCash();
		formOfPaymentData = new FormOfPaymentData();
		expected = new Cash();
	}

	@Test
	public void can_create_cash() {
		// given
		final String freeText = "CASH";

		// when
		boolean canProcess = converter.isApplicable(freeText);

		// then
		Assert.assertEquals(true, canProcess);
	}

	@Test
	public void should_create_cash() {
		// given
		final String freeText = "CASH";

		// when
		formOfPaymentData = converter.createFormOfPaymentData(freeText);
		actual = formOfPaymentData.getCashFop();

		// then
		expected.setCash("CASH");
		Assert.assertEquals(expected.cashText, actual.cashText);
	}

	@Test
	public void can_create_cash_with_b_number() {
		// given
		final String freeText = "CA.B123";

		// when
		boolean canProcess = converter.isApplicable(freeText);

		// then
		Assert.assertEquals(true, canProcess);
	}

	@Test
	public void should_create_cash_with_b_number() {
		// given
		final String freeText = "CASH.B123456789";

		// when
		formOfPaymentData = converter.createFormOfPaymentData(freeText);
		actual = formOfPaymentData.getCashFop();

		// then
		expected.setCash("CASH");
		expected.setBnumber("123456789");
		Assert.assertEquals(expected.bnumber, actual.bnumber);
		Assert.assertEquals(expected.cashText, actual.cashText);
	}

	@Test
	public void should_not_create_non_cash() {

		// given
		final String freeText = "CHECK";

		// when
		boolean actual = converter.isApplicable(freeText);

		// then
		Assert.assertEquals(false, actual);
	}

}