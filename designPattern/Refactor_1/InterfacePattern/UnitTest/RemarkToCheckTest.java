package com.sabre.tds.pnr.conversion;

import org.junit.Before;
import org.junit.Test;

import com.sabre.pnrData.Check;
import com.sabre.pnrData.FormOfPaymentData;

import junit.framework.Assert;

public class RemarkToCheckTest {
	private RemarkToCheck converter;
	private FormOfPaymentData formOfPaymentData;
	private Check expected;
	private Check actual;

	@Before
	public void setUp() throws Exception {
		converter = new RemarkToCheck();
		formOfPaymentData = new FormOfPaymentData();
		expected = new Check();
		actual = new Check();
	}

	@Test
	public void can_create_check() {
		// given
		final String freeText = "CHECK";

		// when
		boolean canProcess = converter.isApplicable(freeText);

		// then
		Assert.assertTrue(canProcess);
	}

	@Test
	public void should_create_check() {
		// given
		final String freeText = "CHECK";

		// when
		formOfPaymentData = converter.createFormOfPaymentData(freeText);
		actual = formOfPaymentData.getCheckFop();

		// then
		expected.setCheck("CHECK");
		Assert.assertEquals(expected.getCheck(), actual.getCheck());
	}

	@Test
	public void can_create_check_with_b_number() {
		// given
		final String freeText = "CK.B123";

		// when
		boolean canProcess = converter.isApplicable(freeText);

		// then
		Assert.assertEquals(true, canProcess);
	}

	@Test
	public void should_create_check_with_b_number() {
		// given
		final String freeText = "CHECK.B123456789";

		// when
		formOfPaymentData = converter.createFormOfPaymentData(freeText);
		actual = formOfPaymentData.getCheckFop();

		// then
		expected.setCheck("CHECK");
		expected.setBnumber("123456789");
		Assert.assertEquals(expected.getBnumber(), actual.getBnumber());
		Assert.assertEquals(expected.getCheck(), actual.getCheck());
	}

	@Test
	public void should_not_create_non_check() {

		// given
		final String freeText = "CASH";

		// when
		boolean actual = converter.isApplicable(freeText);

		// then
		Assert.assertEquals(false, actual);
	}

}