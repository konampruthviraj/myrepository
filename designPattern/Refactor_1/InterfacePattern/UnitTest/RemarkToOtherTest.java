package com.sabre.tds.pnr.conversion;

import org.junit.Before;
import org.junit.Test;

import com.sabre.pnrData.FormOfPaymentData;
import com.sabre.pnrData.OtherFop;

import junit.framework.Assert;

public class RemarkToOtherTest {
	private RemarkToOther converter;
	private FormOfPaymentData formOfPaymentData;
	private OtherFop expected;
	private OtherFop actual;

	@Before
	public void setUp() throws Exception {
		converter = new RemarkToOther();
		formOfPaymentData = new FormOfPaymentData();
		expected = new OtherFop();
		actual = new OtherFop();
	}

	@Test
	public void can_create_other() {
		// given
		final String freeText = "AGT INV";

		// when
		boolean canProcess = converter.isApplicable(freeText);

		// then
		Assert.assertTrue(canProcess);
	}

	@Test
	public void should_create_other() {
		// given
		final String freeText = "AGT INV";

		// when
		formOfPaymentData = converter.createFormOfPaymentData(freeText);
		actual = formOfPaymentData.getOtherFop();

		// then
		expected.setOther("AGT INV");
		Assert.assertEquals(expected.getOther(), actual.getOther());
	}

	@Test
	public void should_create_other_with_b_number() {
		// given
		final String freeText = "AGT INV.B123456789";

		// when
		formOfPaymentData = converter.createFormOfPaymentData(freeText);
		actual = formOfPaymentData.getOtherFop();

		// then
		expected.setOther("AGT INV");
		expected.setBnumber("123456789");
		Assert.assertEquals(expected.getBnumber(), actual.getBnumber());
		Assert.assertEquals(expected.getOther(), actual.getOther());
	}

	@Test
	public void should_not_create_non_other() {

		// given
		final String freeText = "CASH";

		// when
		boolean actual = converter.isApplicable(freeText);

		// then
		Assert.assertEquals(false, actual);
	}

}