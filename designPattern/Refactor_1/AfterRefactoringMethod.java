//THis is test pull request feature
//add another line of code

private FormOfPaymentData remarkToFormOfPayment(RemarkData remark, List<FopData> fopDataList) {
  long uid = new BigInteger(+1, remark.getUniqueIdentifier()).longValue();
  if (fopDataList.isEmpty()) {
    return transformOtherFop(remark.getRemarkText());
  }
  FopRow fopRow = findFopRowForRemarkId(fopDataList, uid);
  return null != fopRow ? transformCreditCardDetails(fopRow) : transformOtherFop(remark.getRemarkText());
}

private FormOfPaymentData transformOtherFop(String remarkText) {
  for (IRemarkToLbtFopDataConverter converter : getConverters()) {
    if (converter.isApplicable(remarkText)) {
      return converter.createFormOfPaymentData(remarkText);
    }
  }
  return createOtherFop(remarkText);
}

private FormOfPaymentData createOtherFop(String remarkText) {
  FormOfPaymentData fop = new FormOfPaymentData();
  OtherFop otherFop = new OtherFop();
  otherFop.setOther(remarkText);
  fop.setOtherFop(otherFop);
  return fop;
}
