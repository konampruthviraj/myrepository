//this file has my new feature
private void populateFopData(PnrTree pnrTree, List<FopData> fopDataList) {
  pnrTree.formsOfPayment.clear();
  //This is the third feature developed in the third branch
  //fposdpfojspjp
  //sdfmsdfpsdjfijasdpiofjasiofjs
  List<RemarkData> remarks = pnrTree.getRemarks();
  for (RemarkData remark : remarks) {
    if (remark.getRemarkType() == RemarkParser.SBFORMOFPAYMENT) {
      long uid = new BigInteger(+1, remark.getUniqueIdentifier()).longValue();
      FopRow fopRow = findFopRowforRemarkId(fopDataList, uid);
      if (fopRow != null) {
        injectCreditCardDetails(pnrTree, fopRow);
      } else {
        String remarkText = remark.getRemarkText();
        if (LbtFormOfPaymentUtil.hasPattern(remarkText, CASH_TYPE)) {
          FormOfPaymentData fopCash = new FormOfPaymentData();
          com.sabre.pnrData.Cash cashFop = new Cash();
          LbtFormOfPaymentUtil.createCashFopFromRemark(cashFop, remarkText);
          fopCash.setCashFop(cashFop);
          pnrTree.formsOfPayment.add(fopCash);
        } else if (LbtFormOfPaymentUtil.hasPattern(remarkText, CHECK_TYPE)) {
          FormOfPaymentData fopCheck = new FormOfPaymentData();
          Check checkFop = new Check();
          LbtFormOfPaymentUtil.createCheckFopFromRemark(checkFop, remarkText);
          fopCheck.setCheckFop(checkFop);
          pnrTree.formsOfPayment.add(fopCheck);
        } else if (LbtFormOfPaymentUtil.hasPattern(remarkText, INVOICE_TYPE)) {
          FormOfPaymentData fopInvoice = new FormOfPaymentData();
          OtherFop invoiceFop = new OtherFop();
          LbtFormOfPaymentUtil.createInvoiceFopFromRemark(invoiceFop, remarkText);
          fopInvoice.setOtherFop(invoiceFop);
          pnrTree.formsOfPayment.add(fopInvoice);
        } else {
          FormOfPaymentData fopOther = new FormOfPaymentData();
          OtherFop otherFop = new OtherFop();
          otherFop.setOther(remarkText);
          pnrTree.formsOfPayment.add(fopOther);
        }
      }
    }
  }
}
